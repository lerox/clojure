(defn two-fns [f g]
    (fn [h] (f (g h))))

(defn doubleIt [a] (* a 2))

(defn plusTwenty [a] (+ a 20))

(println ((two-fns doubleIt plusTwenty) 10))
